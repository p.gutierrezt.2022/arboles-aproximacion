#!/usr/bin/env pythoh3

'''
Cálculo del número óptimo de árboles.
'''

import sys

def compute_trees(trees):

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit

    return production

def compute_all(min_trees, max_trees):

    productions = []

    for numero in range(int(min_trees), int(max_trees) + 1):
        tupla = (numero, compute_trees(numero))
        productions.append(tupla)

    return productions

def read_arguments():

    if len(sys.argv) != 6:
        print("Usage: aprox.py <base_trees> <fruit_per_tree> <reduction> <min> <max>")
        sys.exit(1)

    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree =int(sys.argv[2])
        reduction = int(sys.argv[3])
        min = int(sys.argv[4])
        max = int(sys.argv[5])

    except ValueError:
        print("All arguments must be integers")
        sys.exit(1)
    return base_trees, fruit_per_tree, reduction, min, max


def main():
    global base_trees, fruit_per_tree, reduction

    base_trees, fruit_per_tree, reduction, min, max = read_arguments()
    productions = compute_all(min, max)
    for printeo in productions:
        print(*printeo)

    max_arboles = max_arboles_prod = int()
    best_production = best_trees = int()

    for arb, prod in productions:
        if arb > max_arboles:
            max_arboles = arb
            max_arboles_prod = prod
        if prod > best_production:
            best_production = prod
            best_trees = arb

    print(f"Best production: {best_production}, for {best_trees} trees")

if __name__ == '__main__':
    main()
